# Fabrício de Oliveira Ribeiro
## _Technical Writer_
_São José do Rio Preto_

## Resume 
My goal as a professional is to apply my knowledge of technical writing, user experience, and multimedia content creation to simplify a complex subject into something easy to understand based on the product's target audience.

I have over a year and a half of experience in technical writing in cloud and desktop solutions, using Markdown integrated with Git and GitHub and multimedia content creation like courses and tutorial
videos.

I finished a user experience course at the IGTI institution in 2021 to apply the knowledge to create the best user experience when consuming technical content. This year, I also finished a technical writing course at ESPM institution, improving my area knowledge.

## Experience
**WDG Automation ⏐ An IBM Company**

_2 years and 7 months_

> Technical Writer

November de 2020 - Present (1 year)
_São José do Rio Preto, São Paulo, Brasil_

Criação de documentação técnica, como, manuais, releases notes, troubleshooting, além de criação de cursos e avaliações baseados na documentação técnica.